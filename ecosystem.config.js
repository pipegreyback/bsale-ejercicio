module.exports = {
  apps: [{
    script: 'index.js',
    watch: '.'
  }, {
    script: './service-worker/',
    watch: ['./service-worker']
  }],

  deploy: {
    production: {
      user: 'pip',
      host: '104.131.0.238',
      key: "~/.ssh/server-sshZ",
      ref: 'origin/master',
      repo: 'git@gitlab.com:pipegreyback/bsale-ejercicio.git',
      path: '/home/pip/jobs/bsale-ejercicio',
      'pre-deploy-local': '',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
