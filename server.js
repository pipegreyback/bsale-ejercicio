const express = require('express')
const axios = require('axios')
const cors = require('cors')
const mysql = require('mysql')
const { products } = require('./endpoints')
const app = express()
const port = 3000

const connection = mysql.createConnection({
    host: 'mdb-test.c6vunyturrl6.us-west-1.rds.amazonaws.com',
    user: 'bsale_test',
    password: 'bsale_test',
    database: 'bsale_test'
})

connection.connect()

// connection.query('SELECT * FROM product', (err, rows, fields) => {
//     if (err)  throw err ;
//     console.log(rows[0].url_image);
// })
// connection.end()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors())



app.get('/product', (req, res) => {
    connection.query({sql:'SELECT product.id, product.name, product.price, product.discount, product.url_image, category.name as `category_name`, category.id as `category_id`\
    FROM product\
    INNER JOIN category\
    ON product.category = category.id'}, (err, rows, fields) => {
    if (err)  throw err ;
    res.send(rows)
})
})

app.get('/product/:id', (req, res) => {
    connection.query({sql:`SELECT product.id, product.name, product.price, product.discount, product.url_image, category.name as 'category_name'\
    FROM product\
    INNER JOIN category\
    ON product.category = category.id\
    WHERE product.id = ${req.params.id}`}, (err, rows, fields) => {
    if (err)  throw err ;
    res.send(rows)
})
})

app.get('/product/name/:name', (req, res) => {
    connection.query({sql:`SELECT product.id, product.name, product.price, product.discount, product.url_image, category.name as 'category_name'\
    FROM product\
    INNER JOIN category\
    ON product.category = category.id\
    WHERE product.name = '${req.params.name}'`}, (err, rows, fields) => {
    if (err)  throw err ;
    res.send(rows)
})
})

app.get('/category/:categoryName', (req, res) => {
    connection.query({sql:`SELECT product.id, product.name, product.price, product.discount, product.url_image, category.name as 'category_name'\
    FROM product\
    INNER JOIN category\
    ON product.category = category.id\
    WHERE category.name = '${req.params.categoryName}'`}, (err, rows, fields) => {
    if (err)  throw err ;
    res.send(rows)
})
})

app.get('/category/', (req, res) => {
    connection.query({sql:`SELECT category.name, category.id FROM category`}, (err, rows, fields) => {
    if (err)  throw err ;
    res.send(rows)
})
})


app.listen(port, () => console.log(`Example on port ${port}`))